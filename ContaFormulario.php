<?php	
  session_start();
 if (!isset($_SESSION["email"])){
	   echo "<script>location.href='index';</script>"; 
	 }
 
	$idConta = 0;
	$nome = "";
	$cpf = "";
	$saldo = "";
	
	
	if(isset($_GET["idConta"])){
		
		$idConta = $_GET["idConta"];

		include ("conexao.php");
		if($conexao) { 
		
			$sql = "SELECT * FROM TbConta WHERE idConta = '$idConta';";		
			
			$resultado = mysqli_query($conexao, $sql);
			mysqli_close($conexao);
			
			foreach($resultado as $linha) {		
				$idConta = $linha['idConta'];
				$nome = $linha['nome'];
				$cpf = $linha['cpf'];
				$saldo = $linha['saldo'];
			}
			
		}else{
			echo 'Falha ao conectar: '.mysqli_error();
		}
	}
			
?>

<html>
    <head>
		<meta charset="utf-8">
		<title>Conta</title>
		<link type="text/css" rel="stylesheet" href="css/bootstrap.css"/>
		<link type="text/css" rel="stylesheet" href="css/conta.css"/>
		<link type="text/css" rel="stylesheet" href="css/layout.css"/>
    </head>

    <body>	
	<header>
     <?php
    include_once 'navbar.php'; 
    ?>
		</header>
		<form style="margin-top:60px" id="formConta" action="ContaSalvar" method="post"> 
			<div class="container">
				<div class="row form-group">
					<div class="col-md-12">
						<label for="conta">Nome</label>  
						<input type="hidden" name="idConta" id="idConta" value="<?php echo $idConta ?>" >
						<input class="form-control" name="nome" id="nome" type="text" value="<?php echo $nome ?>">
					</div>			
				</div>
				<div class="row form-group">
					<div class="col-md-12">
						<label for="cpf">CPF</label>
						<input class="form-control" id="cpf" name="cpf" type="text" placeholder="000.000.000-00" maxlength="14" value="<?php echo $cpf ?>">
					</div>			
				</div>	
				<div class="row form-group">
					<div class="col-md-12">
						<label for="saldo">Saldo</label>
						<input class="form-control" id="saldo" name="saldo" type="float" value="<?php echo $saldo ?>">
					</div>			
				</div>	
				
				
				<div class="row form-group">
					<div class="col-md-11">
						<button class="btn btn-success" type="submit" name="action">Salvar</button>
						<button class="btn btn-danger" type="reset" name="action">Cancelar</button>						
					</div>											
					<div class="col-md-1">
						<a class="btn btn-primary" href="ContaTabela">Voltar</a>
					</div>																									
				</div>					
			</div>
		</form >	
		<footer>
     <?php
     include_once 'footer.php'; 
     ?>
		</footer>
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/jquery.maskMoney.min.js"></script>
		<script type="text/javascript" src="js/jquery.mask.js"></script>
		<script type="text/javascript">
		$("#cpf").mask("000.000.000-00");
		</script>
		<script type="text/javascript" src="js/jquery.validate.js"></script>
		<script type="text/javascript" src="js/bootstrap.js"></script>
		<script type="text/javascript" src="js/conta.js"></script>
    </body>
</html>