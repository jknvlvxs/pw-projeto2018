<?php
	session_start();
	
	if (isset($_SESSION["USUARIO"])){
		header('Location: Menu.php');
	}
    ?>
<html>
    <head>
		<meta charset="utf-8">
		<title>Recuperar senha</title>
		<link type="text/css" rel="stylesheet" href="css/bootstrap.css"/>
		<link type="text/css" rel="stylesheet" href="css/loginRecuperar.css"/>
    </head>

    <body id="corpovazio">	
		<div id="areaFormLoginRecuperar" class="centered">
					
			<form id="formLoginRecuperar" action="LoginRecuperarSenha" method="post">
				<div class="container">
		
					<?php
						if(isset($_GET["envio"])){
							if( $_GET["envio"] == 1){
								echo "<div class='alert alert-success' role='alert'>";
								echo "E-mail enviado com sucesso!";
								echo "</div>";
							}else{
								if( $_GET["envio"] == 0){
									echo "<div class='alert alert-danger' role='alert'>";
									echo "Ocorreu um erro ao enviar o email";
									echo "</div>";									
								}
							}
						}
					?>
					
					<div class="alert alert-primary" role="alert">
						Insira seu endereço de e-mail para receber as instruções.
					</div>					
			
					<div class="row form-group">	
						<div class="col-md-12">						
							<label for="email">E-mail</label>  
							<input class="form-control" name="email" id="email" type="email">
						</div>			
					</div>				
					<div class="row form-group">
						<div class="col-md-12">
							<button class="btn btn-primary" type="submit" name="action">Enviar</button>
							<a class="btn btn-secondary" href="index">Login</a>							
						</div>							
					</div>							
				</div>
			</form >	
		<div>
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/bootstrap.js"></script>
		<script type="text/javascript" src="js/jquery.validate.js"></script>
		<script type="text/javascript" src="js/loginRecuperar.js"></script>
    </body>
</html>