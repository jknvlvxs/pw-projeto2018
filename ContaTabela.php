<?php
  session_start();
  if (!isset($_SESSION["email"])){
        echo "<script>location.href='index';</script>"; 
      }
  ?>
<html>
    <head>
		<meta charset="utf-8">
		<title>Conta</title>
		<link type="text/css" rel="stylesheet" href="css/bootstrap.css"/>
		<link type="text/css" rel="stylesheet" href="css/Conta.css"/>
		<link type="text/css" rel="stylesheet" href="css/menu.css"/>
		<link type="text/css" rel="stylesheet" href="css/layout.css"/>
    </head>

    <body>	
		<header>
     <?php
    include_once 'navbar.php'; 
    ?>
		</header>
		<div style="margin:0"  class="table-responsive">				
			<table class="table table-hover">
				<thead>
					<tr>
						<th><a class="btn btn-warning" href="menu">Voltar</a></th>
						<th></th>
						<th class="col-md-2"></th>
						<th></th>
						<th><a class="btn btn-primary" href="ContaFormulario">Novo</a></th>
					</tr>			
					<tr>
						<th>Nome</th>
						<th>CPF</th>
						<th>Saldo</th>
						<th></th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php
						include ("conexao.php");
						if($conexao) { 
							$sql = "SELECT * FROM TbConta;";		
							$resultado = mysqli_query($conexao, $sql);
							mysqli_close($conexao);
							foreach($resultado as $linha){		
								echo"<tr>";			
								echo"<td>".$linha['nome']."</td>";
								echo"<td>".$linha['cpf']."</td>";
								echo"<td>".$linha['saldo']."</td>";
								echo"<td> <a class='btn btn-success' href='ContaFormulario?idConta=".$linha['idConta']."'> Editar </a> </td>";
								echo"<td> <a class='btn btn-danger'  href='ContaExcluir?idConta=".$linha['idConta']."'> Excluir </a> </td>";								
								echo"</tr>";					
							}
						}else{
							echo 'Falha ao conectar: '.mysqli_error();
						}
					?>		
						
				</tbody>
			</table>	
		<div>				

		<footer>
     <?php
     include_once 'footer.php'; 
     ?>
		</footer>
		
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/bootstrap.js"></script>
    </body>
</html>