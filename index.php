<?php
 session_start();
	if (isset($_SESSION["email"])){
        echo "<script>location.href='menu';</script>"; 
	}
	?>
	<html>
    <head>
		<meta charset="utf-8">
		<title>Login</title>
		<link type="text/css" rel="stylesheet" href="css/bootstrap.css"/>
		<link type="text/css" rel="stylesheet" href="css/usuario.css"/>
		<link type="text/css" rel="stylesheet" href="css/login.css"/>
		<link type="text/css" rel="stylesheet" href="css/layout.css"/>
    </head>

    <body id="corpovazio">	
    <header>
     <?php
    include_once 'navbarD.php'; 
    ?>
    </header>
    <form style="margin-top:100px;" id="formUsuario" action="login" method="post">

  <div class="container">
    <label id="logint" for="uname"><b>Usuário</b></label>
    <input type="text" name="email" id="email" required placeholder="exemplo@exemplo.com" title="Digite um email válido" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"><br><br>


    <label id="logint" for="psw"><b>Senha</b></label>
    <input type="password" placeholder="Informe sua senha" name="senha" id="senha" required>

    <button type="submit">Entrar</button>
    <label>
     <!--<input type="checkbox" checked="checked" name="remember"> <label id="logint" for="uname">Lembrar-me</label>-->
       <span class="psw"><a href="UsuarioFormulario">Cadastre-se</a></span> 
    </label>

       <span class="psw"><a href="LoginRecuperar">Esqueceu sua senha? </a></span>
      
  </div>
</form>
<footer>
     <?php
     include_once 'footer.php';
     ?>
    </footer>
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/bootstrap.js"></script>
    </body>
</html>