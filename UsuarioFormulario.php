<?php	
  session_start();
//  if (!isset($_SESSION["email"])){
// 	echo "<script>location.href='index.php';</script>"; 
//   }
	$idUsuario = 0;
	$login = "";
	$senha = "";
	$email = "";
	$situacao = 1;
	$idGrupoAcesso = 0;
	
	
	if(isset($_GET["idUsuario"])){
		
		$idUsuario = $_GET["idUsuario"];

		include ("conexao.php");	
		if($conexao) { 
		
			$sql = "SELECT * FROM TbUsuario WHERE idUsuario = '$idUsuario';";		
			
			$resultado = mysqli_query($conexao, $sql);
			mysqli_close($conexao);
			
			foreach($resultado as $linha) {		
				$idUsuario = $linha['idUsuario'];
				$login = $linha['login'];
				$senha = $linha['senha'];
				$email = $linha['email'];
				$situacao = $linha['situacao'];
				$idGrupoAcesso = $linha['idGrupoAcesso'];			
			}
			
		}else{
			echo 'Falha ao conectar: '.mysqli_error();
		}
	}
			
?>

<html>
    <head>
		<meta charset="utf-8">
		<title>Usuário</title>
		<link type="text/css" rel="stylesheet" href="css/bootstrap.css"/>
		<link type="text/css" rel="stylesheet" href="css/usuario.css"/>
		<link type="text/css" rel="stylesheet" href="css/layout.css"/>
    </head>

    <body>	
	<header>
     <?php
     if (!isset($_SESSION["email"])){
    include_once 'navbarD.php';
    }else{
        include_once 'navbar.php'; 
    }
    
    ?>
		</header>
		<form style="margin-top:60px"  id="formUsuario" action="UsuarioSalvar" enctype="multipart/form-data" method="post" >
			<div class="container">
				<div class="row form-group">
					<div class="col-md-12">
						<label for="login">Usuário</label>  
						<input type="hidden" name="idUsuario" id="idUsuario" value="<?php echo $idUsuario ?>" >
						<input class="form-control" name="login" id="login" type="text" value="<?php echo $login ?>">
					</div>			
				</div>
				<div class="row form-group">
					<div class="col-md-12">
						<label for="senha">Senha</label>
						<input class="form-control" id="senha" name="senha" type="password" value="<?php echo $senha ?>">
					</div>			
				</div>	
				<div class="row form-group">
					<div class="col-md-12">
						<label for="email">E-mail</label>
						<input class="form-control" id="email" name="email" type="text" value="<?php echo $email ?>">
					</div>			
				</div>	
				
  
				<div class="row form-group">
					<div class="col-md-12">
						<legend class="col-form-label">Situação</legend>

						<div class="form-check">
							<input class="form-check-input" type="radio" name="situacao" id="situacaoHabilitado" value="1" <?php if($situacao == 1){echo 'checked';} ?> >
							<label class="form-check-label" for="situacaoHabilitado">Habilitado</label>
						</div>
						
						<div class="form-check">
							<input class="form-check-input" type="radio" name="situacao" id="situacaoBloqueado" value="0" <?php if($situacao == 0){echo 'checked';} ?>>
							<label class="form-check-label" for="situacaoBloqueado">Bloqueado</label>
						</div>
					</div>
				</div>

				<div class="row form-group">		
					<div class="col-md-12">
						<label for="idGrupoAcesso">Grupo de Acesso</label>
						<select id="idGrupoAcesso" name="idGrupoAcesso" class="form-control" required>				  
							<?php
							  include ("conexao.php");
							
								if($conexao) { 
									$sql = "SELECT * FROM TbGrupoAcesso;";		
									$resultado = mysqli_query($conexao, $sql);
									mysqli_close($conexao);
									
									if($idGrupoAcesso == 0){
										echo "<option value='' disabled selected>Selecione um grupo de acesso</option>";
									}
							foreach($resultado as $linha){	
							if($linha['idGrupoAcesso'] == $idGrupoAcesso){
							echo "<option selected value='".$linha['idGrupoAcesso']."'>".$linha['descricao']."</option>";
										}
										else{
											echo "<option value='".$linha['idGrupoAcesso']."'>".$linha['descricao']."</option>";
										}
										
									}
								}else{
									echo 'Falha ao conectar: '.mysqli_error();
								}
							?>	
					
						</select>
						
					</div>								
				</div>	
				
				<div class="row form-group">		
					<div class="col-md-12">
						<label for="foto">Foto</label>
						<input id="foto" name="foto" type="file" class="form-control" value="<?php echo 'img/'.$caminho ?>">	
					</div>
				</div>
				
						
				<div class="row form-group">
					<div class="col-md-11">
						<button class="btn btn-success" type="submit" name="action">Salvar</button>
						<button class="btn btn-danger" type="reset" name="action">Cancelar</button>						
					</div>											
					<div class="col-md-1">
						<a class="btn btn-primary" href="UsuarioTabela">Voltar</a>
					</div>																									
				</div>					
			</div>
		</form >	
		<footer>
     <?php
     include_once 'footer.php'; 
     ?>
		</footer>
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/bootstrap.js"></script>
		<script type="text/javascript" src="js/jquery.validate.js"></script>
		<script type="text/javascript" src="js/usuario.js"></script>
    </body>
</html>