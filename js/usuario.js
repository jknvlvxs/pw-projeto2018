﻿
$("#formUsuario").validate({
       rules:{
           login:{
               required:true
           }, 
           senha:{
               required:true
           }, 
           email:{
               required:true,
			   email: true			   
           }, 
           situacao:{
               required:true
           }, 
           grupoAcesso:{
               required:true
           } 		   
       }, 
       messages:{
           login:{
               required:"Campos obrigatório"
           },
           senha:{
               required:"Campos obrigatório"
           },
           email:{
               required:"Campo obrigatório",
			   email:"E-mail inválido"
           },
           situacao:{
               required:"Campo obrigatório"
           },
           grupoAcesso:{
               required:"Campo obrigatório"
           }		   
       }
});


