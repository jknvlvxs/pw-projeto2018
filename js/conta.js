$("#formConta").validate({
    rules:{
        nome:{
            required:true
        }, 
        cpf:{
            required:true
        }, 
        saldo:{
            required:true,
        }, 	   
    }, 
    messages:{
        nome:{
            required:"Campos obrigatório"
        },
        cpf:{
            required:"Campos obrigatório"
        },
        saldo:{
            required:"Campo obrigatório",
        }	   
    }
});


