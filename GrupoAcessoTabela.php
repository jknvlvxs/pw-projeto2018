<?php
  session_start();
  if (!isset($_SESSION["email"])){
        echo "<script>location.href='index';</script>"; 
      }
  ?>
<html>
    <head>
		<meta charset="utf-8">
		<title>Grupo de Acesso</title>
		<link type="text/css" rel="stylesheet" href="css/bootstrap.css"/>
		<link type="text/css" rel="stylesheet" href="css/grupoacesso.css"/>
		<link type="text/css" rel="stylesheet" href="css/layout.css"/>
    </head>

    <body>	
		<header>
     <?php
    include_once 'navbar.php'; 
    ?>
		</header>
		
		<div style="margin:0" class="table-responsive">					
			<table class="table table-hover">
				<thead>
					<tr>
						<th class="col-md-10"><a class="btn btn-warning" href="menu">Voltar</a></th>
						<th class="col-md-1"></th>
						<th class="col-md-1"><a class="btn btn-primary" href="GrupoAcessoFormulario">Novo</a></th>
					</tr>			
					<tr>
						<th class="col-md-10">Descrição</th>
						<th class="col-md-1"></th>
						<th class="col-md-1"></th>
					</tr>
				</thead>
				<tbody>
					<?php
					    include ("conexao.php");
							
						if($conexao) { 
							$sql = "SELECT * FROM TbGrupoAcesso;";		
							$resultado = mysqli_query($conexao, $sql);
							mysqli_close($conexao);
							foreach($resultado as $linha){		
								echo"<tr>";			
								echo"<td>".$linha['descricao']."</td>";
								echo"<td> <a class='btn btn-success' href='GrupoAcessoFormulario?idGrupoAcesso=".$linha['idGrupoAcesso']."'> Editar </a> </td>";
								echo"<td> <a class='btn btn-danger'  href='GrupoAcessoExcluir?idGrupoAcesso=".$linha['idGrupoAcesso']."'> Excluir </a> </td>";								
								echo"</tr>";					
							}
						}else{
							echo 'Falha ao conectar: '.mysqli_error();
						}
					?>		
						
				</tbody>
			</table>	
		<div>				
		<footer>
     <?php
     include_once 'footer.php'; 
     ?>
		</footer>
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/bootstrap.js"></script>
    </body>
</html>