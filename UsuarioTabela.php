<?php
  session_start();
  if (!isset($_SESSION["email"])){
        echo "<script>location.href='index';</script>"; 
      }
  ?>
<html>
    <head>
		<meta charset="utf-8">
		<title>Usuário</title>
		<link type="text/css" rel="stylesheet" href="css/bootstrap.css"/>
		<link type="text/css" rel="stylesheet" href="css/usuario.css"/>
		<link type="text/css" rel="stylesheet" href="css/layout.css"/>
    </head>

    <body>	
		<header>
     <?php
    include_once 'navbar.php'; 
    ?>
		</header>
		
		<div style="margin:0" class="table-responsive">					
			<table class="table table-hover">
				<thead>
					<tr>
						<th class="col-md-3"><a class="btn btn-warning" href="menu">Voltar</a></th>
						<th class="col-md-3"></th>
							<th class="col-md-3"></th>
						<th class="col-md-3"></th>
						<th class="col-md-3"><a class="btn btn-primary" href="UsuarioFormulario">Novo</a></th>
					</tr>			
					<tr>
						<th class="col-md-3"></th>
						<th class="col-md-3">Usuário</th>
						<th class="col-md-3">E-mail</th>
						<th class="col-md-3"></th>
						<th class="col-md-3"></th>
					</tr>
				</thead>
				<tbody>
				   
					<?php
					if ($_SESSION["idGrupoAcesso"] == 1){
					    include ("conexao.php");
						if($conexao) { 
							$sql = "SELECT * FROM TbUsuario;";		
							$resultado = mysqli_query($conexao, $sql);
							mysqli_close($conexao);
							foreach($resultado as $linha){		
								echo"<tr>";	
								echo"<td> <img src='img/".$linha['foto']."' width='60px' height='60px'> </img> </td>";
								echo"<td>".$linha['login']."</td>";
								echo"<td>".$linha['email']."</td>";
								echo"<td> <a class='btn btn-success' href='UsuarioFormulario?idUsuario=".$linha['idUsuario']."'> Editar </a> </td>";
								echo"<td> <a class='btn btn-danger'  href='UsuarioExcluir?idUsuario=".$linha['idUsuario']."'> Excluir </a> </td>";								
								echo"</tr>";					
							}
						}else{
							echo 'Falha ao conectar: '.mysqli_error();
						}
					}else{
					    include ("conexao.php");
						if($conexao) { 
							$sql = "SELECT * FROM TbUsuario WHERE idUsuario = ". $_SESSION["idUsuario"].";";		
							$resultado = mysqli_query($conexao, $sql);
							mysqli_close($conexao);
							foreach($resultado as $linha){		
								echo"<tr>";	
								echo"<td> <img src='img/".$linha['foto']."' width='60px' height='60px'> </img> </td>";
								echo"<td>".$linha['login']."</td>";
								echo"<td>".$linha['email']."</td>";
								echo"<td> <a class='btn btn-success' href='UsuarioFormulario?idUsuario=".$linha['idUsuario']."'> Editar </a> </td>";
								echo"<td> <a class='btn btn-danger'  href='UsuarioExcluir?idUsuario=".$linha['idUsuario']."'> Excluir </a> </td>";								
								echo"</tr>";					
							}
						}else{
							echo 'Falha ao conectar: '.mysqli_error();
						}
					}
					
						
					?>		
						
				</tbody>
			</table>	
		<div>				
		<footer>
     <?php
     include_once 'footer.php'; 
     ?>
		</footer>
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/bootstrap.js"></script>
    </body>
</html>