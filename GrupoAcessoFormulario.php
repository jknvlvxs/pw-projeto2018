<?php	
  session_start();
if (!isset($_SESSION["email"])){
	echo "<script>location.href='index';</script>"; 
  }
	$idGrupoAcesso = 0;
	$descricao = "";
	
	if(isset($_GET["idGrupoAcesso"])){
		
		$idGrupoAcesso = $_GET["idGrupoAcesso"];
        
		include ("conexao.php");
		

		if($conexao) { 
		
			$sql = "SELECT * FROM TbGrupoAcesso WHERE idGrupoAcesso = '$idGrupoAcesso';";		
			
			$resultado = mysqli_query($conexao, $sql);
			mysqli_close($conexao);
			
			foreach($resultado as $linha) {		
				$idGrupoAcesso = $linha['idGrupoAcesso'];
				$descricao = $linha['descricao'];		
			}
			
		}else{
			echo 'Falha ao conectar: '.mysqli_error();
		}
	}
		
?>

<html>
    <head>
		<meta charset="utf-8">
		<title>Grupo de Acesso</title>
		<link type="text/css" rel="stylesheet" href="css/bootstrap.css"/>
		<link type="text/css" rel="stylesheet" href="css/grupoacesso.css"/>
		<link type="text/css" rel="stylesheet" href="css/layout.css"/>
    </head>
		<header>
     <?php
    include_once 'navbar.php'; 
    ?>
		</header>
    <body>	
		<div class="container">		
			<form style="margin-top:60px" id="formGrupoAcesso" action="GrupoAcessoSalvar" method="post">

				<div class="row form-group">
					<div class="col-md-12">
						<label for="descricao">Descrição</label>  
						<input type="hidden" name="idGrupoAcesso" id="idGrupoAcesso" value="<?php echo $idGrupoAcesso ?>" >
						<br>
						<input class="form-control" name="descricao" id="descricao" type="text" value="<?php echo $descricao ?>" >
					</div>
				</div>				
				<div class="row form-group">
					<div class="col-md-11">
						<button class="btn btn-success" type="submit" id="salvar">Salvar</button>
						<button class="btn btn-danger" type="reset" id="cancelar">Cancelar</button>						
					</div>											
					<div class="col-md-1">
						<a class="btn btn-primary" href="GrupoAcessoTabela">Voltar</a>					
					</div>																
				</div>					
			</form >	
		</div>
		<footer>
     <?php
     include_once 'footer.php'; 
     ?>
		</footer>
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/bootstrap.js"></script>
		<script type="text/javascript" src="js/jquery.validate.js"></script>
		<script type="text/javascript" src="js/grupoacesso.js"></script>
    </body>
</html>